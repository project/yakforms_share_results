# What is this module ?

Yakforms Share Results is an enhancement module for [Yakforms](https://www.drupal.org/project/yakforms).

This module allows users to give access to form results to other authenticated users through the form "Edit" page.

# Requirements

This module requires the following to work properly :

* Yakforms, which defines the form1 node type.
* yakforms_feature, which defines the user autocomplete view.

Both of these can be downloaded on the [development Git repo](https://framagit.org/framasoft/framaforms).

# Credits

Like Yakforms, this module makes heavy use of [Webform](https://www.drupal.org/project/webform/) for Drupal 7.

This module was originally developped by non-profit [Framasoft](https://framasoft.org/en).
